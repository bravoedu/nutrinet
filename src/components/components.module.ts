import { NgModule } from '@angular/core';
import { ViewquestionComponent } from './viewquestion/viewquestion';
@NgModule({
	declarations: [ViewquestionComponent],
	imports: [],
	exports: [ViewquestionComponent]
})
export class ComponentsModule {}
