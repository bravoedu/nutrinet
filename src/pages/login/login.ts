import { Storage } from '@ionic/storage';
import { Component, OnDestroy } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, MenuController } from 'ionic-angular';
import { User } from '../../providers/user/user';
//import { ShoppingCartProvider } from '../../providers/api/shopping-cart/shopping-cart';
import { TokenStorageService } from '../../providers/providers';


/** 
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage implements OnDestroy {

  public account: { username: string, password: string } = {
    username: '', //'adminmachete',
    password: ''//'machete'
  };

  isLoading: boolean = false

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public user: User,
    public menuCtrl: MenuController,
    private tokenStorageService: TokenStorageService,
    private store: Storage
  ) {
  }

  ionViewDidLoad() {
    this.menuCtrl.swipeEnable(false)
    //console.log('ionViewDidLoad LoginPage');
  }

  ionViewWillLeave() {
    this.menuCtrl.swipeEnable(true)
  }

  doLogin() {
    this.isLoading = true
    this.user.login(this.account).subscribe((resp: any) => {
      //console.log(resp);
      this.isLoading = false;        
      this.store.ready().then(
        ready =>{
          this.store.set('userData',{
            firstName: resp.firstName,
            lastName: resp.lastName,
            userId: resp.userId 
          })
        }
      );
      this.navCtrl.push('CardsPage');
    }, (err) => {
      this.isLoading = false
      this.confirmationToast({msg: 'Verifique sus crendenciales. Usuario y/o Password incorrectos'});
    });

  }

  goToSignIn(){
    this.navCtrl.push('SignUpPage')
  }

  confirmationToast({ msg }: any){

    let toastEdit = this.toastCtrl.create({
      message: msg,
      position: 'top',
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toastEdit.present();
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.account = {
      username: '', //'adminmachete',
      password: ''//'machete'
    };
  }

}