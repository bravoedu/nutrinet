import { Token } from './../../models/token';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';
// class Token {
//   expires : string; // Date
//   issued : string; // Date
//   access_token : string;
//   client_id : string;
//   emailAddress : string;
//   expires_in : string;
//   firstName : string;
//   lastName : string;
//   refresh_token : string;
//   token_type : string;
//   userId : string

//   constructor(){}
// }


@Injectable()
export class TokenStorageService {

  constructor(private storage: Storage) { }
   

  public getToken(){ 
    //let token = JSON.parse(localStorage.getItem('session'));
    return Observable.fromPromise(this.storageGetValue('access_token'));
  }

  public deleteToken(): Promise<void>{
    //SlocalStorage.removeItem('session');
    return this.storage.ready().then(
      (success) => {
        this.storage.clear()
      })
  }

  public isAuthenticated(): Promise<boolean> {
    //let token = JSON.parse(localStorage.getItem('session'));
    return this.storage.ready().then(
      (success) => {
        return this.isStorageKey('access_token')
      })
    // return tokenNotExpired(null, token);

  }

  public setToken(token: string): Promise<boolean> {
    //localStorage.setItem('session', token)
    return this.storage.ready().then(
      (success) => {
        return this.storageSet('session',token)
      })

    //this.storage.set('session', token)
  }

  public getUserId(): Promise<string> { // string
    //let token = JSON.parse(localStorage.getItem('session'))
    return this.storage.ready().then(
      (success) => {
        return this.storageGetValue('userId')
      }
    )
  }

  public getUserEmail(): Promise<string>{
    return this.storage.ready().then(
      (success) => {
        return this.storageGetValue('emailAddress');
      }
    );
  }

  

  async storageSet(nameToken, token): Promise<boolean> { // boolean
    return await this.storage.ready().then(
      success => {
        return this.storage.set(nameToken, token).then(
          (success) => { console.log('storageSet',true) ; return true },
          (error) => { console.log('storageSet',false) ; return false }
        )
      }
    )

  }

  async storageGetValue(key) { // string

    return await this.storage.ready().then(() => {
      
      return this.storage.get('session').then(
        (session) => {
          let sessionParse: Token = JSON.parse(session)
          if ( sessionParse !== null ){
            //console.log(`storageGet ${key} : ${sessionParse[key]}`)
            return sessionParse[key]
          } else {
            console.log(`storageGet ${key} : '' `)
            return ''
          }
        },
        (error) => { console.log('storageGetValue Error',false) ; return false }
      )
    });
  }

  async isStorageKey(key){ //boolean
    return await this.storage.ready().then(() => {
      return this.storage.get('session').then(
        (session) => { 
          let sessionParse: Token = JSON.parse(session)
          if ( sessionParse !== null && sessionParse[key] ) {
            console.log(`isStorageKey ${key} : ${sessionParse[key]}`)
            return true
          } else {
            console.log(`isStorageKey ${key} : '' `)
            return false
          }
        },
        (error) => { console.log('isStorageKey Error',false) ; return false }
      )
    })

  }



}
