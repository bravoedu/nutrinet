import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProportionPage } from './proportion';
import { TranslateModule } from '@ngx-translate/core';
import { MultiPickerModule } from 'ion-multi-picker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ProportionPage,
  ],
  imports: [
    MultiPickerModule,
    FormsModule,
    ReactiveFormsModule,
    IonicPageModule.forChild(ProportionPage),
    TranslateModule.forChild()    
  ],
  exports:[
    ProportionPage
  ]
})
export class ProportionPageModule {}
