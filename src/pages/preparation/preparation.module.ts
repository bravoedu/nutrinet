import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PreparationPage } from './preparation';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    PreparationPage,
  ],
  imports: [
    IonicPageModule.forChild(PreparationPage), 
    TranslateModule.forChild()    
  ],
  exports:[
    PreparationPage
  ]
})
export class PreparationPageModule {}
