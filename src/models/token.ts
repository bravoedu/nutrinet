export class Token {
  expires : string; // Date
  issued : string; // Date
  access_token : string;
  client_id : string;
  emailAddress : string;
  expires_in : string;
  firstName : string;
  lastName : string;
  refresh_token : string;
  token_type : string;
  userId : string

   constructor(){}

}