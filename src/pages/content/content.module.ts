import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewquestionComponent } from '../../components/viewquestion/viewquestion'

import { ContentPage } from './content';

@NgModule({
  declarations: [
    ContentPage,
    ViewquestionComponent
  ],
  imports: [
    IonicPageModule.forChild(ContentPage),
    TranslateModule.forChild()
  ],
  exports: [
    ContentPage
  ]
})
export class ContentPageModule { }
