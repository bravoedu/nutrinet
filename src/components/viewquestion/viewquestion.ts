import { Component, Input, OnInit, EventEmitter, Output, ViewChild, ElementRef, QueryList, ViewChildren } from '@angular/core';
import { Slides, NavController, Content, App, LoadingController } from 'ionic-angular';
import { Title } from '@angular/platform-browser';
import { TokenStorageService } from '../../providers/providers';
import { AnswerProvider } from '../../providers/api/answer/answer';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ViewquestionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'viewquestion',
  templateUrl: 'viewquestion.html'
})
export class ViewquestionComponent implements OnInit{

  @Input('pregunta') pregunta: any;
  @Input ("responseData") responseData: any;
  @Input('title') title: string;
  @Input('ansewacceso') ansewacceso: any;
  @Input('idSurvey') idSurvey: any;

  @Output() respuesta: EventEmitter<string> = new EventEmitter<string>();
  @Output() goToNext: EventEmitter<string> = new EventEmitter<string>();

  type : string = '';
  isRequired: boolean;
  respuestas = Array();
  response = new Response();
  respuestaEcha;

  constructor(
    public navCtrl: NavController,
    private tokenStorageService: TokenStorageService, 
    public loadingController: LoadingController, 
    public app: App,
    public answerProvider: AnswerProvider,
    private storage: Storage 
    ){}

  ngOnInit() {
    
    //console.log(this.pregunta);
    
    this.type = this.pregunta.type
    this.isRequired = this.pregunta.required;
    
    //voy armando la posible respuesta
    this.response.pollId = this.idSurvey;
    this.response.questionId = this.pregunta.id;
    this.response.type = this.pregunta.type;    
    this.response.other = "";
    //loader.dismiss();

    if(this.pregunta.respuesta !== null){
      this.respuestaEcha = this.pregunta.respuesta;
      this.goToNext.emit('1');
    }
    else{      
      let aux: any;
      aux = {"selectedAnswer":null, "answer":null};
      this.respuestaEcha = aux;
    }

    console.log(this.respuestaEcha);
    console.log(this.pregunta);

    
  }

  mcqAnswer(answer:any){
    let idPregunta;

    this.armarRespuesta(answer);
    this.guardarRespuesta();    
    
    if(answer.haveQuestion===true){
      idPregunta = answer.questions[0].id;
    }
    else{      
      idPregunta = this.getNextQuestion(answer.id);
      //console.log('sin pregunta anidada, id pregunta: '+ idPregunta);
    } 
    this.respuesta.emit(idPregunta);
  }

  getNextQuestion(id){
    for(let i=0; i<this.ansewacceso.length; i++){
      if(this.ansewacceso[i].actual == id)
        return this.ansewacceso[i].sig
    }
  }

  changInput(data,id,event,answer){
    answer.id = id;
    answer.input = event.value;
    answer.haveQuestion = false;
    this.armarRespuesta(answer);
    this.guardarRespuesta();  
    if(event.value !== undefined)
        this.respuestas.push(Array(id,event.value));
      else
        if(event.day !== undefined)
          this.respuestas.push(Array(id,event));
      if(this.isRequired){
        if((data.placeholders !== undefined)&&(data.placeholders.length > 0)){        
        if(data.placeholders.length === this.respuestas.length){
          this.goToNext.emit(id);
        }
        }else{
          this.goToNext.emit(id);
        }
      }else{
        this.goToNext.emit(id);
      }
  }

  checkRequired(){
    if(this.isRequired===false)
      this.goToNext.emit('1');
  }

  guardarRespuesta(){
    
    this.responseData = {};
      let loader = this.loadingController.create({
      content: "Cargando"
    });

    this.tokenStorageService.getUserId().then(
      (userId) =>{        
        if(userId != ''){//si tiene id de usuario guardo la respuesta
          //console.log(userId);
          this.response.answerDetailId=0;
          this.response.answerId=0;
          this.response.userId = userId;
          var utc = new Date().toJSON().slice(0,10);    
          let time = new Date();
          utc = utc +' '+ time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();  
          this.response.startDate = utc;

          console.log(this.response);

          this.answerProvider.addResponse(this.response).subscribe(
            (surveie: any) => {
              //console.log(surveie);
              this.response.answerDetailId = surveie.AnswereDetailId;
              this.response.answerId = surveie.AnswerId;

              this.storage.get('respuestas').then((val) => {
                this.respuesta = val;
                this.respuestas.push(this.response);
                this.storage.set('respuestas', this.respuestas);
              });
            },(error:any)=>{
              //error
            }
          );
          
        }else{//si no lo saco al login
          loader.dismiss();
          this.navCtrl.setRoot('LoginPage');
        }

    });

    /*
    {
      "answerDetailId":1650,
      "answerId": 141,
      "parentId": "2c9252781d83bf99cb4aebe8aa5e2f48",
      "userId": 87,
      "startDate": "2019-01-24 17:01:44.000",
      "questionId": "cdb103177efd3200173b0a74e35193aa",
      "type":"select",
      "selectedAnswer":"dc00f331eeefb97cf8d52d92c013a5e2",
      "answerText":"Señor",
      "isOtherAnswer":"false",
      "other":""
    }
    */

  }

  armarRespuesta(respuesta){
    //console.log(respuesta);
    this.response.selectedAnswer = respuesta.id;    
    this.response.answerText = respuesta.value;
    this.response.isOtherAnswer = respuesta.haveQuestion;
  }

}

export class Response { 
  answerDetailId: any;
  answerId: any;
  pollId: any;
  userId: any;
  startDate: any;
  questionId: any;
  type: any;
  selectedAnswer: any;
  answerText: any;
  isOtherAnswer: any;
  other: any;
}
