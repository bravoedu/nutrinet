
import { Injectable } from '@angular/core';
import { Api } from '../api';

/*
  Generated class for the SurveyProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SurveyProvider {

  constructor(public api: Api) {}

  getAllSurveies() {
    return this.api.get('polls/getpolls');
  }

  getAllSurveiesPublished() {
    let result = this.api.get('polls/getpollspublished');
    //console.log(result);
    return result;
    
  }

  getById(id) {
    return this.api.get(`polls/getpollspublished/${id}`);
  }

}
