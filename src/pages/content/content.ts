import { Component, ViewChildren, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController,Content } from 'ionic-angular';
import { SurveyProvider } from './../../providers/api/survey/survey';
import { AnswerProvider } from '../../providers/api/answer/answer';
import { TokenStorageService } from '../../providers/providers';
import { Storage } from '@ionic/storage';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { ResourceLoader } from '@angular/compiler';

@IonicPage()
@Component({
  selector: 'page-content',
  templateUrl: 'content.html'
})
export class ContentPage implements OnInit{
  @ViewChild(Slides) slides: Slides;
  idSurvey;
  responseData: any;
  surveie: any;
  title;
  questions;
  activo:boolean = false;
  questionsacceso = new Array;
  ansewacceso = new Array;
  index = 0;
  answer;

  @ViewChildren('endOfScroll') myComponents: Content;

  constructor( 
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public surveyProvider: SurveyProvider,
    public answerProvider: AnswerProvider, 
    private tokenStorageService: TokenStorageService, 
    public loadingController: LoadingController, 
    private storage: Storage){ 

    this.idSurvey = this.navParams.get('id');
    this.responseData = {};
    
    let loader = this.loadingController.create({content: "Cargando"});
    loader.present();

    this.surveyProvider.getById(this.idSurvey).subscribe(
      (surveie: any) => {
        this.surveie = surveie.formData.pages[0];
        this.title = this.surveie.name;
        this.questions = this.surveie.elements;

        this.tokenStorageService.getUserId().then(
          (userId) =>{  
            
            //console.log(userId);
                  
            if(userId != ''){//si tiene id de usuario guardo la respuesta
              this.answerProvider.getResponsesByPoll(this.idSurvey,userId).subscribe(
                (answer: any) => {
                  this.answer = answer;

                  console.log(this.answer);
                  
                  //recorro las preguntas y armo el hilo
                  this.armarLista(this.questions,null,true);                  
                  storage.set('preguntas', this.questions);
                  //console.log(this.questionsacceso);
                  loader.dismiss();
                }
              );

            }
          }
        );

        
    },
      ( error ) => {
        loader.dismiss();
        this.navCtrl.setRoot('LoginPage');
       }
    );

  }

  armarLista(questions: any, sig, raiz):void{
    
    for(let i=0; i < questions.length; i++){
      
      let actual = questions[i].question;
      if(questions[i].question === undefined){
         actual = questions[i];
      }

      
      if((questions[i+1]!==undefined)&&(questions[i+1].question !== undefined)){
        sig = questions[i+1].question.id;
      }else
      {
        if((questions[i+1]===undefined)&&(raiz===true)){
          sig = null;
        }
      }

      let respuesta = this.getRespuesta(this.answer, actual.id);
      actual.respuesta = respuesta;

      this.questionsacceso.push(actual);

      //pregunto si tiene lista de respuestas predeterminada
      if(actual.offeredAnswers !== undefined){ 
        //si tiene, llamo armar lista de respuestas          
        for(let j=0; j < actual.offeredAnswers.length; j++){
          //pregunto si la respuesta tiene preguntas asociadas
          if( ( actual.offeredAnswers[j].haveQuestion===true ) && ( actual.offeredAnswers[j].questions !== undefined ) ){
            //si tiene preguntas asociadas, las agrego a la lista de preguntas
            this.armarLista(actual.offeredAnswers[j].questions,sig,false);
          }else{
            //console.log('llega');
            //si la respuesta no tiene preguntas anidadas, tengo que registrar cual seria la siguiente pregunta
            let nodo = new Nodo;
            nodo.actual = actual.offeredAnswers[j].id;
            nodo.sig = sig; 
            nodo.ant = i;
            this.ansewacceso.push(nodo);
          }
        }
      }
    
    }
  }

  getRespuesta(respuestas, id){
   
    for(let i=0; i < respuestas.length; i++){
      if(respuestas[i].responseData[id] !== undefined)
        return respuestas[i].responseData[id].questionResponse;
    }

    return null;

  }

  ngOnInit() {
    //console.log(this.pregunta);
    this.slides.lockSwipes(true);
  }

  setSiguienteSlider(event){    
    let idSlider:number = this.getIdQuestion(event);
    
    //console.log(idSlider); 
    //if(idSlider > 1)
      //idSlider--;
    
    if(idSlider===null){
      //llege al final de las preguntas
      let loader = this.loadingController.create({content: "finalizando cuestionario"});
      loader.present();
      setTimeout( () => {
        this.navCtrl.push('CardsPage');
        loader.dismiss();
        } , 400 );
    }
    
    this.slides.lockSwipes(false);
    this.slides.slideTo(idSlider, 500);
    this.slides.lockSwipes(true);
  }

  getIdQuestion(id){
    for(let i = 0; i<this.questionsacceso.length; i++){
      if(this.questionsacceso[i].id === id)
        return i;
    }
    return null;
  }

  goToNextSlider(event){

    this.activo = true;    
    //console.log(event);
    
    //this.slides.lockSwipes(false);
    //this.slides.slideNext();
    //this.slides.lockSwipes(true);
  }

  nextSlider(){
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.slides.lockSwipes(true);
    this.activo = false;
  }

  goToList(){
    this.navCtrl.setRoot('CardsPage');
  }  

}

export class Nodo { 
  actual: any;
  sig: any;
  ant: any;
}