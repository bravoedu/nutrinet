import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import { HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/do';



import 'rxjs/add/operator/mergeMap';
import { TokenStorageService } from '../providers';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(public tokenStorageService:TokenStorageService) {}
  
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {  
    let getToken = this.tokenStorageService.getToken();

    return getToken.mergeMap(
      (token) => {
        request = request.clone({
          setHeaders: {
            Authorization: `Bearer ${ token }`
          }
        });

        return next.handle(request).do(event => {}, err => {
          if (err instanceof HttpErrorResponse && err.status == 401) {
              // handle 401 errors
              this.tokenStorageService.deleteToken().then(
                (success) => {
                  console.log('Session Clear')
                }
              )
          }
      });;
      }
    )

    
  }
}