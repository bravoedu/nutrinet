import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { SurveyProvider } from './../../providers/api/survey/survey';
import { TokenStorageService } from '../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-cards',
  templateUrl: 'cards.html'
})
export class CardsPage {
  cardItems: any[];
  
  surveies: any[] = [];
  responses: any[] = []
  loadProgress: number; 
  // Bandera para cargar el ProgressBar
  flag:boolean = false;
  responseData: any;
  

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public surveyProvider: SurveyProvider,
    private tokenStorageService: TokenStorageService, 
    public loadingController: LoadingController){
      
      this.cardItems = [];
      this.responseData = {};
        let loader = this.loadingController.create({
          content: "loading"
      });

      loader.present();
      this.surveyProvider.getAllSurveiesPublished().subscribe(
        (surveies: any) => {
            this.surveies = surveies;
            //console.log(this.surveies);
            
            loader.dismiss();
        },
          ( error ) => {
            loader.dismiss();
            this.navCtrl.setRoot('LoginPage');
          }
      ); 
  }

  selectSurvey(id){
    this.navCtrl.push('ContentPage',{id:id});
  }

}
