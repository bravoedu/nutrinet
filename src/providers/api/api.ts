import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Environments } from '../environments.constanst';

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class Api {
 

  constructor(public http: HttpClient) {
  }

  get(endpoint: string, params?: any, reqOpts?: any) {
    if (!reqOpts) {
      reqOpts = {
        params: new HttpParams()
      };
    }

    // Support easy query params for GET requests
    if (params) {
      reqOpts.params = new HttpParams();
      for (let k in params) {
        reqOpts.params = reqOpts.params.set(k, params[k]);
      }
    }

    //console.log(Environments.API_ENDPOINT + '/' + endpoint);
    //console.log(reqOpts);
    
    return this.http.get(Environments.API_ENDPOINT + '/' + endpoint, reqOpts);
  }

  post(endpoint: string, body: any, reqOpts?: any) {
    return this.http.post(Environments.API_ENDPOINT + '/' + endpoint, body, reqOpts);
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    return this.http.put(Environments.API_ENDPOINT + '/' + endpoint, body, reqOpts);
  }

  delete(endpoint: string, reqOpts?: any) {
    return this.http.delete(Environments.API_ENDPOINT + '/' + endpoint, reqOpts);
  }

  patch(endpoint: string, body: any, reqOpts?: any) {
    return this.http.patch(Environments.API_ENDPOINT + '/' + endpoint, body, reqOpts);
  }
}