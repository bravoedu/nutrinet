import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SaltProportionPage } from './salt-proportion';
import { TranslateModule } from '@ngx-translate/core';
import { MultiPickerModule } from 'ion-multi-picker';

@NgModule({
  declarations: [
    SaltProportionPage,
  ],
  imports: [
    MultiPickerModule,
    IonicPageModule.forChild(SaltProportionPage),
    TranslateModule.forChild()
  ],
  exports: [
    SaltProportionPage
  ]
})
export class SaltProportionPageModule { }
