import { Component, ViewChildren } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MultiPicker } from 'ion-multi-picker';

/**
 * Generated class for the ProportionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({ name: "proportion", segment: "proportion" })
@Component({
  selector: 'page-proportion',
  templateUrl: 'proportion.html',
})
export class ProportionPage {

  public proportion = "A";
  public quantity = 4;
  public proportionAmount = 500;
  public fruit = {
    col1: {
      columnIndex: 0,
      text: "1",
      value: "1"
    }, col2: {
      columnIndex: 1,
      text: "1/4",
      value: "1/4"
    }, col3: {
      columnIndex: 2,
      text: "Muy pequeño",
      value: "Muy pequeño"
    }
  };

  public firstValue = {val1: "",val2: "",val3: ""};
  public secondValue = {val1: "",val2: "",val3: ""};
  public thirdValue = {val1: "",val2: "",val3: ""};

  public simpleColumns = [
    {
      name: 'col1',
      options: [
        { text: '1', value: '1' },
        { text: '2', value: '2' },
        { text: '3', value: '3' },
        { text: '4', value: '4' },
        { text: '5', value: '5' }
      ]
    }, {
      name: 'col2',
      options: [
        { text: '1/4', value: '1/4' },
        { text: '1/3', value: '1/3' },
        { text: '1/2', value: '1/2' },
        { text: '2/3', value: '2/3' },
        { text: '3/4', value: '3/4' },
        { text: '1', value: '1' }
      ]
    }, {
      name: 'col3',
      options: [
        { text: 'Muy pequeño', value: 'Muy pequeño' },
        { text: 'Pequeño', value: 'Pequeño' },
        { text: 'Medio', value: 'Medio' },
        { text: 'Grande', value: 'Grande' },
        { text: 'Muy grande', value: 'Muy grande' },
        { text: 'Demasiado grande', value: '2-1-2' }
      ]
    }
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.firstValue = this.getValues(this.fruit,this.simpleColumns[0])
    this.secondValue = this.getValues(this.fruit,this.simpleColumns[1])
    this.thirdValue = this.getValues(this.fruit,this.simpleColumns[2])
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProportionPage');
  }

  changeProportion(value) {
    this.proportion = value;
  }

  loadData(data) {
    this.firstValue = this.getValues(data,this.simpleColumns[0])
    this.secondValue = this.getValues(data,this.simpleColumns[1])
    this.thirdValue = this.getValues(data,this.simpleColumns[2])
  }

  getValues(data,col) {
    let index = col.options.findIndex(val => val.value == data[col.name].value)
    let values = { val1:"",val2:"",val3:"" };
    values.val1 = index - 1 >= 0 ?
      col.options[index - 1].text : ""

    values.val2 = col.options[index].text

    values.val3 = index + 1 < col.options.length ?
      col.options[index + 1].text : ""

    return values;

  }
}
