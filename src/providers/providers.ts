import { Api } from './api/api';
import { Items } from './items/items';
import { Settings } from './settings/settings';
//import { ModelNameSubjectService } from './subjects/model-name-subject/model-name.subject.service';
//import { ModelApi } from './api/model-name-api/model-api';
import { TokenStorageService } from './auth/token-storage.service';
import { User } from './user/user';
//import { ProductProvider } from "./api/product/product";

export {
    Api,
    Settings,
    //ModelNameSubjectService,
    //ModelApi,
    TokenStorageService,
    User,
    Items
    //ProductProvider
};
