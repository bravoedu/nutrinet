import { Injectable } from '@angular/core';
import { Api } from '../api';


/*
  Generated class for the AnswerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AnswerProvider {

  constructor(public api: Api) {    
  }

  postResponse(response){
     return this.api.post('Answers/create',response);
  }

  getResponses(userId){
    return this.api.get(`answers/getanswer-byuser/${userId}`)
  }

  updateResponse(response, id){
    return this.api.post('Answers/update',response);
  }

  addResponse(response){
    return this.api.post('Answers/add',response);
  }

  getResponsesByPoll(pollId,userId){
    return this.api.get(`Answers/get-answers-by-poll/${pollId}/user/${userId}`)
  }
  



}
